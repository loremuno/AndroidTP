package com.tp.loginregistro.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tp.loginregistro.R;
import com.tp.loginregistro.adapters.UsersRecyclerAdapter;
import com.tp.loginregistro.modelo.Usuario;
import com.tp.loginregistro.sql.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lalit on 10/10/2016.
 */

public class ListaUsuariosActivity extends AppCompatActivity {

    private AppCompatActivity activity = ListaUsuariosActivity.this;
    private AppCompatTextView textViewNombre;
    private RecyclerView recyclerViewUsusarios;
    private List<Usuario> listUsuarios;
    private UsersRecyclerAdapter usersRecyclerAdapter;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_usuario);
        getSupportActionBar().setTitle("");
        initViews();
        initObjects();

    }

    /**
     * This method is to initialize views
     */
    private void initViews() {
        textViewNombre = (AppCompatTextView) findViewById(R.id.textViewNombre);
        recyclerViewUsusarios = (RecyclerView) findViewById(R.id.recyclerViewUsuarios);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        listUsuarios = new ArrayList<>();
        usersRecyclerAdapter = new UsersRecyclerAdapter(listUsuarios);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewUsusarios.setLayoutManager(mLayoutManager);
        recyclerViewUsusarios.setItemAnimator(new DefaultItemAnimator());
        recyclerViewUsusarios.setHasFixedSize(true);
        recyclerViewUsusarios.setAdapter(usersRecyclerAdapter);
        databaseHelper = new DatabaseHelper(activity);

        String emailFromIntent = getIntent().getStringExtra("EMAIL");
        textViewNombre.setText(emailFromIntent);

        getDataFromSQLite();
    }

    /**
     * This method is to fetch all user records from SQLite
     */
    private void getDataFromSQLite() {
        // AsyncTask is used that SQLite operation not blocks the UI Thread.
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                listUsuarios.clear();
                listUsuarios.addAll(databaseHelper.getAllUsuario());

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                usersRecyclerAdapter.notifyDataSetChanged();
            }
        }.execute();
    }
}
