package com.tp.loginregistro.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.tp.loginregistro.R;
import com.tp.loginregistro.helpers.InputValidacion;
import com.tp.loginregistro.sql.DatabaseHelper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private final AppCompatActivity activity = LoginActivity.this;

    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutClave;

    private TextInputEditText textInputEditTextEmail;
    private TextInputEditText textInputEditTextClave;

    private AppCompatButton appCompatButtonLogin;

    private AppCompatTextView textViewLinkRegistro;

    private InputValidacion inputValidacion;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    /**
     * This method is to initialize views
     */
    private void initViews() {

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutClave = (TextInputLayout) findViewById(R.id.textInputLayoutClave);

        textInputEditTextEmail = (TextInputEditText) findViewById(R.id.textInputEditTextEmail);
        textInputEditTextClave = (TextInputEditText) findViewById(R.id.textInputEditTextClave);

        appCompatButtonLogin = (AppCompatButton) findViewById(R.id.appCompatButtonLogin);

        textViewLinkRegistro = (AppCompatTextView) findViewById(R.id.textViewLinkRegistro);

    }

    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonLogin.setOnClickListener(this);
        textViewLinkRegistro.setOnClickListener(this);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        databaseHelper = new DatabaseHelper(activity);
        inputValidacion = new InputValidacion(activity);

    }

    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.appCompatButtonLogin:
                verifyFromSQLite();
                break;
            case R.id.textViewLinkRegistro:
                // Navigate to RegistroActivity
                Intent intentRegister = new Intent(getApplicationContext(), RegistroActivity.class);
                startActivity(intentRegister);
                break;
        }
    }

    /**
     * This method is to validate the input text fields and verify login credentials from SQLite
     */
    private void verifyFromSQLite() {
        if (!inputValidacion.isInputEditTextFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_mensaje_email))) {
            return;
        }
        if (!inputValidacion.isInputEditTextEmail(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_mensaje_email))) {
            return;
        }
        if (!inputValidacion.isInputEditTextFilled(textInputEditTextClave, textInputLayoutClave, getString(R.string.error_mensaje_email))) {
            return;
        }

        if (databaseHelper.checkUsuario(textInputEditTextEmail.getText().toString().trim()
                , textInputEditTextClave.getText().toString().trim())) {


            Intent accountsIntent = new Intent(activity, ListaUsuariosActivity.class);
            accountsIntent.putExtra("EMAIL", textInputEditTextEmail.getText().toString().trim());
            emptyInputEditText();
            startActivity(accountsIntent);


        } else {
            // Snack Bar to show success message that record is wrong
            Snackbar.make(nestedScrollView, getString(R.string.error_valido_email_clave), Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        textInputEditTextEmail.setText(null);
        textInputEditTextClave.setText(null);
    }
}
