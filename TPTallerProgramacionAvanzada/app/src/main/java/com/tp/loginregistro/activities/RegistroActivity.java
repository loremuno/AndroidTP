package com.tp.loginregistro.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.tp.loginregistro.R;
import com.tp.loginregistro.helpers.InputValidacion;
import com.tp.loginregistro.modelo.Usuario;
import com.tp.loginregistro.sql.DatabaseHelper;

/**
 * Created by lalit on 8/27/2016.
 */
public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private final AppCompatActivity activity = RegistroActivity.this;

    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutNombre;
    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutClave;
    private TextInputLayout textInputLayoutConfirmarClave;

    private TextInputEditText textInputEditTextNombre;
    private TextInputEditText textInputEditTextEmail;
    private TextInputEditText textInputEditTextClave;
    private TextInputEditText textInputEditTextConfirmarClave;

    private AppCompatButton appCompatButtonRegistro;
    private AppCompatTextView appCompatTextViewLoginLink;

    private InputValidacion inputValidacion;
    private DatabaseHelper databaseHelper;
    private Usuario usuario;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        getSupportActionBar().hide();

        initViews();
        initListeners();
        initObjects();
    }

    /**
     * This method is to initialize views
     */
    private void initViews() {
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        textInputLayoutNombre = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutClave = (TextInputLayout) findViewById(R.id.textInputLayoutClave);
        textInputLayoutConfirmarClave = (TextInputLayout) findViewById(R.id.textInputLayoutConfirmarClave);

        textInputEditTextNombre = (TextInputEditText) findViewById(R.id.textInputEditTextName);
        textInputEditTextEmail = (TextInputEditText) findViewById(R.id.textInputEditTextEmail);
        textInputEditTextClave = (TextInputEditText) findViewById(R.id.textInputEditTextClave);
        textInputEditTextConfirmarClave = (TextInputEditText) findViewById(R.id.textInputEditTextConfirmarClave);

        appCompatButtonRegistro = (AppCompatButton) findViewById(R.id.appCompatButtonRegistro);

        appCompatTextViewLoginLink = (AppCompatTextView) findViewById(R.id.appCompatTextViewLoginLink);

    }

    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonRegistro.setOnClickListener(this);
        appCompatTextViewLoginLink.setOnClickListener(this);

    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        inputValidacion = new InputValidacion(activity);
        databaseHelper = new DatabaseHelper(activity);
        usuario = new Usuario();

    }


    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.appCompatButtonRegistro:
                postDataToSQLite();
                break;

            case R.id.appCompatTextViewLoginLink:
                finish();
                break;
        }
    }

    /**
     * This method is to validate the input text fields and post data to SQLite
     */
    private void postDataToSQLite() {
        if (!inputValidacion.isInputEditTextFilled(textInputEditTextNombre, textInputLayoutNombre, getString(R.string.error_mensaje_nombre))) {
            return;
        }
        if (!inputValidacion.isInputEditTextFilled(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_mensaje_email))) {
            return;
        }
        if (!inputValidacion.isInputEditTextEmail(textInputEditTextEmail, textInputLayoutEmail, getString(R.string.error_mensaje_email))) {
            return;
        }
        if (!inputValidacion.isInputEditTextFilled(textInputEditTextClave, textInputLayoutClave, getString(R.string.error_mensaje_clave))) {
            return;
        }
        if (!inputValidacion.isInputEditTextMatches(textInputEditTextClave, textInputEditTextConfirmarClave,
                textInputLayoutConfirmarClave, getString(R.string.error_clave_incorrecta))) {
            return;
        }

        if (!databaseHelper.checkUsuario(textInputEditTextEmail.getText().toString().trim())) {

            usuario.setNombre(textInputEditTextNombre.getText().toString().trim());
            usuario.setEmail(textInputEditTextEmail.getText().toString().trim());
            usuario.setClave(textInputEditTextClave.getText().toString().trim());

            databaseHelper.agregarUsuario(usuario);

            // Snack Bar to show success message that record saved successfully
            Snackbar.make(nestedScrollView, getString(R.string.success_mensaje), Snackbar.LENGTH_LONG).show();
            emptyInputEditText();


        } else {
            // Snack Bar to show error message that record already exists
            Snackbar.make(nestedScrollView, getString(R.string.error_email_existe), Snackbar.LENGTH_LONG).show();
        }


    }

    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
        textInputEditTextNombre.setText(null);
        textInputEditTextEmail.setText(null);
        textInputEditTextClave.setText(null);
        textInputEditTextConfirmarClave.setText(null);
    }
}
