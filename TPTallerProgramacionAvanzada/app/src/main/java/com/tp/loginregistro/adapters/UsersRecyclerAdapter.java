package com.tp.loginregistro.adapters;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tp.loginregistro.R;
import com.tp.loginregistro.modelo.Usuario;

import java.util.List;

/**
 * Created by lalit on 10/10/2016.
 */

public class UsersRecyclerAdapter extends RecyclerView.Adapter<UsersRecyclerAdapter.UserViewHolder> {

    private List<Usuario> listUsuarios;

    public UsersRecyclerAdapter(List<Usuario> listUsuarios) {
        this.listUsuarios = listUsuarios;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_recycler, parent, false);

        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.textViewNombre.setText(listUsuarios.get(position).getNombre());
        holder.textViewEmail.setText(listUsuarios.get(position).getEmail());
        holder.textViewClave.setText(listUsuarios.get(position).getClave());
    }

    @Override
    public int getItemCount() {
        Log.v(UsersRecyclerAdapter.class.getSimpleName(),""+ listUsuarios.size());
        return listUsuarios.size();
    }


    /**
     * ViewHolder class
     */
    public class UserViewHolder extends RecyclerView.ViewHolder {

        public AppCompatTextView textViewNombre;
        public AppCompatTextView textViewEmail;
        public AppCompatTextView textViewClave;

        public UserViewHolder(View view) {
            super(view);
            textViewNombre = (AppCompatTextView) view.findViewById(R.id.textViewNombre);
            textViewEmail = (AppCompatTextView) view.findViewById(R.id.textViewEmail);
            textViewClave = (AppCompatTextView) view.findViewById(R.id.textViewClave);
        }
    }


}
